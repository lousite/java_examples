package com.lousite;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Jim on 1/9/2016.
 */
public class Helper {
    static public Reader openReader(String fileName) throws IOException {
    	System.out.println("input file = " + Paths.get(fileName).toString());
        return Files.newBufferedReader(Paths.get(fileName));
    }

    static public Writer openWriter(String fileName) throws IOException {
    	System.out.println("output file = " + Paths.get(fileName).toString());
        return Files.newBufferedWriter(Paths.get(fileName));
    }

}

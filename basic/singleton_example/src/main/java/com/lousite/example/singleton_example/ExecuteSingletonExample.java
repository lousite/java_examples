package com.lousite.example.singleton_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExecuteSingletonExample {

	public static void main(String[] args) {
		SpringApplication.run(ExecuteSingletonExample.class, args);
		
		//SingletonExample_1_static firstInstance = SingletonExample_1_static.getInstanceOfSingletonExample();
		//SingletonExample_2_LazyLoad firstInstance = SingletonExample_2_LazyLoad.getInstanceOfSingletonExample();
		SingletonExample_3_MakeThreadSafe firstInstance = SingletonExample_3_MakeThreadSafe.getInstanceOfSingletonExample();
		
		System.out.println("This is the address of instance:" + firstInstance);
		
		//SingletonExample_1_static secondInstance = SingletonExample_1_static.getInstanceOfSingletonExample();
		//SingletonExample_2_LazyLoad secondInstance = SingletonExample_2_LazyLoad.getInstanceOfSingletonExample();
		SingletonExample_3_MakeThreadSafe secondInstance = SingletonExample_3_MakeThreadSafe.getInstanceOfSingletonExample();
		
		System.out.println("This is the address of instance:" + secondInstance);
		
		if(firstInstance == secondInstance) {
			System.out.println("The two instances are the same");
		}
		
		// Try creating another instance of the singleton
		//SingletonExample_3_SingleThread test = new SingletonExample_3_SingleThread();
	}

}

package com.lousite.example.singleton_example;

public class SingletonExample_1_static {
	
	// This class is in charge of creating it's own instance because it is private
	private static SingletonExample_1_static instanceOfSingletonExample = new SingletonExample_1_static();
	
	// Create the constructor as private to avoid anything outside of the class using it
	private SingletonExample_1_static() {
		
	}
	
	// Only allow others to get the instance not modify, singletons are immutable
	public static SingletonExample_1_static getInstanceOfSingletonExample() {
		return instanceOfSingletonExample;
	}
	
}

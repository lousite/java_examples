package com.lousite.example.singleton_example;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExecuteSingletonDerbyDatabaseExample {

	public static void main(String[] args) {
		SpringApplication.run(ExecuteSingletonDerbyDatabaseExample.class, args);
		
		final String CREATE_TABLE_SQL_QUERY = "CREATE TABLE DERBY_TEST_TABLE (ID INT, StreetName VARCHAR(30),"
				+ " City VARCHAR(20))";
		
		SingletonExample_4_UsingDerbyDatabase firstDatabaseConnectionInstance = SingletonExample_4_UsingDerbyDatabase.getInstanceOfSingletonExample();
		long timeBefore = 0;
		long timeAfter  = 0;
		System.out.println("This is the address of instance:" + firstDatabaseConnectionInstance);
		
		timeBefore = System.currentTimeMillis();
		Connection dbConnection = firstDatabaseConnectionInstance.getInstanceOfSingletonDatabaseConnection();
		timeAfter  = System.currentTimeMillis();
		
		System.out.println("Initial database connection time in milliseconds: " + (timeAfter - timeBefore));
		
		Statement databaseQueryStatement;
		try {
			databaseQueryStatement = dbConnection.createStatement();
			int rowCount = firstDatabaseConnectionInstance.getRowCountInTable("DERBY_TEST_TABLE");
			if (rowCount <= 0) {
				int count = databaseQueryStatement.executeUpdate(CREATE_TABLE_SQL_QUERY);
				System.out.println("Table created - count=" + count);
			}	
			databaseQueryStatement.close();			
		} catch (SQLException dbException) {
			dbException.printStackTrace();
		}
		
		
		timeBefore = System.currentTimeMillis();
		dbConnection = firstDatabaseConnectionInstance.getInstanceOfSingletonDatabaseConnection();
		timeAfter  = System.currentTimeMillis();
		
		System.out.println("Request database connection time in milliseconds: " + (timeAfter - timeBefore));
	}

}

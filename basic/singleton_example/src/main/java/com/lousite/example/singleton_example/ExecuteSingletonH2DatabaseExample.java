package com.lousite.example.singleton_example;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExecuteSingletonH2DatabaseExample {

	public static void main(String[] args) {
		SpringApplication.run(ExecuteSingletonH2DatabaseExample.class, args);
		
		final String CREATE_TABLE_SQL_QUERY = "CREATE TABLE H2_TEST_TABLE (ID INT, StreetName VARCHAR(30),"
				+ " City VARCHAR(20))";
		
		SingletonExample_5_UsingH2Database firstDatabaseConnectionInstance = SingletonExample_5_UsingH2Database.getInstanceOfSingletonExample();
		long timeBefore = 0;
		long timeAfter  = 0;
		System.out.println("This is the address of instance:" + firstDatabaseConnectionInstance);
		
		// Let's connect to the H2 database and get an idea of how long
		timeBefore = System.currentTimeMillis();
		Connection dbConnection = firstDatabaseConnectionInstance.getInstanceOfSingletonDatabaseConnection();
		timeAfter  = System.currentTimeMillis();
		
		System.out.println("Initial database connection time in milliseconds: " + (timeAfter - timeBefore));
		
		Statement databaseQueryStatement;
		try {
			databaseQueryStatement = dbConnection.createStatement();
			int rowCount = firstDatabaseConnectionInstance.getRowCountInTable("DERBY_TEST_TABLE");
			if (rowCount <= 0) {
				int count = databaseQueryStatement.executeUpdate(CREATE_TABLE_SQL_QUERY);
				System.out.println("Table created - count=" + count);
			}	
			databaseQueryStatement.close();			
		} catch (SQLException dbException) {
			dbException.printStackTrace();
		}
		
		// Let's try connecting to the H2 database again and get an idea of how long it will take
		//  Since the connection exist within the singleton, it should be super quick to get it db connection 
		timeBefore = System.currentTimeMillis();
		dbConnection = firstDatabaseConnectionInstance.getInstanceOfSingletonDatabaseConnection();
		timeAfter  = System.currentTimeMillis();
		
		System.out.println("Request database connection time in milliseconds: " + (timeAfter - timeBefore));;
	}

}

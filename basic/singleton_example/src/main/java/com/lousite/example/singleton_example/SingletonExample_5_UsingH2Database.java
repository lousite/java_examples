package com.lousite.example.singleton_example;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SingletonExample_5_UsingH2Database {
	
	// This class is in charge of creating it's own instance because it is private, make volatile to make sure it remains a 
	// singleton if there are changes within the JVM
	private static volatile SingletonExample_5_UsingH2Database instanceOfSingletonExample = null;
	private static volatile Connection instanceOfSingletonDatabaseConnection = null;
	private static final String DATABASE_URL = "jdbc:h2:~/mainDB";
	private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String USER = "test";
    private static final String PASSWORD = "test";


    Statement statement = null;
    String myStatement;
    ResultSet rs;
    DatabaseMetaData meta;
	
	// Create the constructor as private to avoid anything outside of the class using it
	private SingletonExample_5_UsingH2Database() {
		
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		if (instanceOfSingletonDatabaseConnection != null) {
			throw new RuntimeException("Database connection error");
		}
		
		if (instanceOfSingletonExample != null) {
			throw new RuntimeException("Avoid multiple instances being created by using getInstanceOfSingletonExample method");
		}
	
    }
	
	
	// Only allow others to get the instance not modify, singletons are immutable
	public static SingletonExample_5_UsingH2Database getInstanceOfSingletonExample() {
		
		// The synchronized is helping to make thread safe, not putting in on method level and adding within null check
		//   allows us to only synchronize only when creating instance - making it more efficient 
		if (instanceOfSingletonExample == null) {
			synchronized(SingletonExample_5_UsingH2Database.class) {
				// This will allow us to load this when we need it, not at startup
				if (instanceOfSingletonExample == null) {
					instanceOfSingletonExample = new SingletonExample_5_UsingH2Database();
				}	
			}
		}
		
		return instanceOfSingletonExample;
	}
	
	public Connection getInstanceOfSingletonDatabaseConnection() {

		if (instanceOfSingletonDatabaseConnection == null) {
			synchronized(SingletonExample_5_UsingH2Database.class) {
				if (instanceOfSingletonDatabaseConnection == null) {
					try {
						instanceOfSingletonDatabaseConnection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
						System.out.println("Connected to database successfully...");
					} catch (SQLException dbException) {
						dbException.printStackTrace();
					}				
				}	
			}
		}
		
		return instanceOfSingletonDatabaseConnection;
	}	
	
	public Integer getRowCountInTable(String tableName) {
		
		int rowCount = 0;
		System.out.println("Getting row count");

		DatabaseMetaData meta;
		try {
			meta = instanceOfSingletonDatabaseConnection.getMetaData();
			ResultSet rs = meta.getTables(null, null, tableName, new String[]{"TABLE"});  // <--- Checks for existence of table "EXTRAS"
			rowCount = rs.getFetchSize();
			System.out.println("Row count:" + rowCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return rowCount;
	}
	
}

package com.lousite.example.singleton_example;

public class SingletonExample_3_MakeThreadSafe {
	
	// This class is in charge of creating it's own instance because it is private, make volatile to make sure it remains a 
	// singleton if there are changes within the JVM
	private static volatile SingletonExample_3_MakeThreadSafe instanceOfSingletonExample = null;
	
	// Create the constructor as private to avoid anything outside of the class using it
	private SingletonExample_3_MakeThreadSafe() {
		// Avoid something else from using reflective on this class
		if (instanceOfSingletonExample != null ) {
			System.out.println("Instance has been created");
			throw new RuntimeException("Avoid multiple instances being created by using getInstanceOfSingletonExample method");
		}
	}	
	
	// Only allow others to get the instance not modify, singletons are immutable
	public static SingletonExample_3_MakeThreadSafe getInstanceOfSingletonExample() {
		
		// The synchronized is helping to make thread safe, not putting in on method level and adding within null check
		//   allows us to only synchronize only when creating instance - making it more efficient 
		if (instanceOfSingletonExample == null) {
			synchronized(SingletonExample_3_MakeThreadSafe.class) {
				// This will allow us to load this when we need it, not at startup
				if (instanceOfSingletonExample == null) {
					instanceOfSingletonExample = new SingletonExample_3_MakeThreadSafe();
				}	
			}
		}
		
		return instanceOfSingletonExample;
	}
	
}

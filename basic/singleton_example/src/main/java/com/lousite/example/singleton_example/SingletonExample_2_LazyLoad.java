package com.lousite.example.singleton_example;

public class SingletonExample_2_LazyLoad {
	
	// This class is in charge of creating it's own instance because it is private
	private static SingletonExample_2_LazyLoad instanceOfSingletonExample = null;
	
	// Create the constructor as private to avoid anything outside of the class using it
	private SingletonExample_2_LazyLoad() {
		
	}
	
	// Only allow others to get the instance not modify, singletons are immutable
	public static SingletonExample_2_LazyLoad getInstanceOfSingletonExample() {
		
		// This will allow us to load this when we need it, not at startup
		if (instanceOfSingletonExample == null) {
			instanceOfSingletonExample = new SingletonExample_2_LazyLoad();
		}
		
		return instanceOfSingletonExample;
	}
	
}

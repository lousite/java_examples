package com.lousite.example.ehcacheAndH2.loader;

import com.lousite.example.ehcacheAndH2.model.Users;
import com.lousite.example.ehcacheAndH2.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class Loader {

    @Autowired
    UsersRepository usersRepository;

    @PostConstruct
    public void load() {
        List<Users> usersList = getList();
        for(Users user : usersList) {
        	usersRepository.save(user);
        }
        //usersRepository.save(usersList.get(0));
    }

    public List<Users> getList() {
        List<Users> usersList = new ArrayList<>();
        usersList.add(new Users("Lou", "Tech Team", 60L));
        usersList.add(new Users("Kelly", "Team Who", 70L));
        return usersList;
    }
}

package com.lousite.example.ehcacheAndH2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EhcacheAndH2Application {

	/*
	 * https://www.youtube.com/watch?v=cWqNeANzEz0&t=920s
	 * 
	 * http://127.0.0.1:8080/rest/users/Lou
	 */
	public static void main(String[] args) {
		SpringApplication.run(EhcacheAndH2Application.class, args);
	}

}

package com.lousite.example.ehcacheAndH2.repository;

import com.lousite.example.ehcacheAndH2.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {
    Users findByName(String name);
}

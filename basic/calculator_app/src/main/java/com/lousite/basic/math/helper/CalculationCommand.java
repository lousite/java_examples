package com.lousite.basic.math.helper;

/**
 * An enumeration to help define the type of calculation command to process
 */
enum CalculationCommmand {
    Add,
    Subtract,
    Multiply,
    Divide
}

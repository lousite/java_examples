package com.lousite.basic.math.function;

import com.lousite.basic.math.CalculatorInterface;
import com.lousite.basic.math.helper.CalculateBase;

/**
 * Created a class to handle the add process
 */
public class Add extends CalculateBase implements CalculatorInterface{
	
	// Constructs
    public Add(){}
    public Add(double leftVal, double rightVal){
        super(leftVal, rightVal);
    }

    @Override
    public void calculate(){
        double value = getLeftVal() + getRightVal();
        setResult(value);
    }


    /**
     * Apply required methods passed down from implementation of the interface 
     */
    //@Override
    public String getKeyword() {
        return "add";
    }
    
    //@Override
    public char getSymbol() {
        return '+';
    }

    //@Override
    public double doCalculation(double leftVal, double rightVal) {
        setLeftVal(leftVal);
        setRightVal(rightVal);
        calculate();

        return getResult();

    }
}


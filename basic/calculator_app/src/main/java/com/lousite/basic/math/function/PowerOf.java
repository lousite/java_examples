package com.lousite.basic.math.function;

import com.lousite.basic.math.CalculatorInterface;

/**
 * Created a class to handle the power of process
 */
public class PowerOf implements CalculatorInterface {
	
	/**
     * Apply required methods passed down from implementation of the interface 
     */
    //@Override
    public String getKeyword() {
        return "power";
    }

    //@Override
    public char getSymbol() {
        return '^';
    }

    //@Override
    public double doCalculation(double leftVal, double rightVal) {
        return Math.pow(leftVal, rightVal);
    }
}

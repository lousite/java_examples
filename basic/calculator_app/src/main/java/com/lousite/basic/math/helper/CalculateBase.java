package com.lousite.basic.math.helper;

/**
 * Create a base class to be used by all calculate type/classes
 */
public abstract class CalculateBase {
	// Define fields needed for all calculate types
    private double leftVal;
    private double rightVal;
    private double result;

    // Getter and setters 
    public double getLeftVal() {return leftVal;}
    public void setLeftVal(double leftVal) {this.leftVal = leftVal;}
    public double getRightVal() {return rightVal;}
    public void setRightVal(double rightVal) {this.rightVal = rightVal;}
    public double getResult() { return result;}
    public void setResult(double result) {this.result = result;}

    // Constructs
    public CalculateBase(){}
    public CalculateBase(double leftVal, double rightVal) {
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }
    
    /*
     *  Create an abstract calculate method (that will also force the class to be abstract), 
     *  this will force the classes extending this base class to implement the calculate
     *  method in a way to fit its need.
     */
    public abstract void calculate();

}

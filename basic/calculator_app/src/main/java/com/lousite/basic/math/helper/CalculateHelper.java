package com.lousite.basic.math.helper;

import com.lousite.basic.math.function.Add;
import com.lousite.basic.math.function.Divide;
import com.lousite.basic.math.function.Multiplie;
import com.lousite.basic.math.function.Subtract;
import com.lousite.math.exception.InvalidStatementException;

/**
 * Created by Jim on 10/18/2015.
 */
public class CalculateHelper {
	// Define constants, these will be static and shared across instances of this class
    private static final char ADD_SYMBOL = '+';
    private static final char SUBTRACT_SYMBOL = '-';
    private static final char MULTIPLY_SYMBOL = '*';
    private static final char DIVIDE_SYMBOL = '/';

    // Define the work fields needed to do calculations
    private CalculationCommmand mathCommandToPerform;
    private double leftValue;
    private double rightValue;
    private double result;

    public void process(String calculationParameters) throws InvalidStatementException {
        // Example input calculation parameter string <command value_one value_two> = add 1.0 2.0
        String[] parts = calculationParameters.split(" ");
        if(parts.length != 3)
            throw new InvalidStatementException("Incorrect number of fields", calculationParameters);

        String commandString = parts[0]; // example value = add, subtract, multiply, etc..... 

        try {
            leftValue = Double.parseDouble(parts[1]);   // example value = 1.0
            rightValue = Double.parseDouble(parts[2]);  // example value = 2.0
        } catch (NumberFormatException e) {
            throw new InvalidStatementException("Non-numeric data", calculationParameters, e);
        }
        
        // Get the type of calculate action
        setCommandFromString(commandString);
        if(mathCommandToPerform == null)
            throw new InvalidStatementException("Invalid command", calculationParameters);

        // Using the calculation action, instantiate the calculation action/object 
        CalculateBase calculator = null;
        switch (mathCommandToPerform) {
            case Add:
                calculator = new Add(leftValue, rightValue);
                break;
            case Subtract:
                calculator = new Subtract(leftValue, rightValue);
                break;
            case Multiply:
                calculator = new Multiplie(leftValue, rightValue);
                break;
            case Divide:
                calculator = new Divide(leftValue, rightValue);
                break;
        }
 
        // Execute the calculate method which was defined in each calculation type class 
        calculator.calculate();
        result = calculator.getResult();


    }

    private void setCommandFromString(String commandString) {
        // add -> MathCommand.Add

        if(commandString.equalsIgnoreCase(CalculationCommmand.Add.toString()))
        	mathCommandToPerform = CalculationCommmand.Add;
        else if(commandString.equalsIgnoreCase(CalculationCommmand.Subtract.toString()))
        	mathCommandToPerform = CalculationCommmand.Subtract;
        else if(commandString.equalsIgnoreCase(CalculationCommmand.Multiply.toString()))
        	mathCommandToPerform = CalculationCommmand.Multiply;
        else if(commandString.equalsIgnoreCase(CalculationCommmand.Divide.toString()))
        	mathCommandToPerform = CalculationCommmand.Divide;
    }

    @Override
    public String toString() {
        // 1.0 + 2.0 = 3.0
        char symbol = ' ';
        switch(mathCommandToPerform) {
            case Add:
                symbol = ADD_SYMBOL;
                break;
            case Subtract:
                symbol = SUBTRACT_SYMBOL;
                break;
            case Multiply:
                symbol = MULTIPLY_SYMBOL;
                break;
            case Divide:
                symbol = DIVIDE_SYMBOL;
                break;
        }

        StringBuilder sb = new StringBuilder(20);
        sb.append(leftValue);
        sb.append(' ');
        sb.append(symbol);
        sb.append(' ');
        sb.append(rightValue);
        sb.append(" = ");
        sb.append(result);

        return sb.toString();
    }



}

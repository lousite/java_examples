package com.lousite.basic.math.function;

import com.lousite.basic.math.helper.CalculateBase;

/**
 * Created a class to handle the multiple process
 */
public class Multiplie extends CalculateBase {
	
	// Constructs
    public Multiplie() {}
    public Multiplie(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }
    
    
    @Override
    public void calculate() {
        double value = getLeftVal() * getRightVal();
        setResult(value);;
    }
}


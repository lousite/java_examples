package com.lousite.basic.math.function;

import com.lousite.basic.math.helper.CalculateBase;

/**
 * Created a class to handle the subtract process
 */
public class Subtract extends CalculateBase {
	
	// Constructs
    public Subtract() {}
    public Subtract(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }
    
    
    @Override
    public void calculate(){
        double value = getLeftVal() - getRightVal();
        setResult(value);;
    }
}


package com.lousite.basic.math.helper;

import com.lousite.basic.math.CalculatorInterface;

/**
 * Created by Jim on 11/16/2015.
 */
public class DynamicHelper {
	
	// 
    private CalculatorInterface[] handlers;

    // Constructs
    public DynamicHelper(CalculatorInterface[] handlers) {
        this.handlers = handlers;
    }

    public String process(String statement) {
        // IN: add 1.0 2.0
        // OUT: 1.0 + 2.0 = 3.0

        String[] parts = statement.split(CalculatorInterface.SEPARATOR);
        String keyword = parts[0]; // add

        CalculatorInterface theHandler = null;

        for(CalculatorInterface handler:handlers) {
            if(keyword.equalsIgnoreCase(handler.getKeyword())) {
                theHandler = handler;
                break;
            }
        }

        double leftVal = Double.parseDouble(parts[1]); // 1.0
        double rightVal = Double.parseDouble(parts[2]); // 2.0

        double result = theHandler.doCalculation(leftVal, rightVal);

        StringBuilder sb = new StringBuilder(20);
        sb.append(leftVal);
        sb.append(' ');
        sb.append(theHandler.getSymbol());
        sb.append(' ');
        sb.append(rightVal);
        sb.append(" = ");
        sb.append(result);

        return sb.toString();

    }
}


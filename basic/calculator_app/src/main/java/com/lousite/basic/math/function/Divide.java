package com.lousite.basic.math.function;

import com.lousite.basic.math.helper.CalculateBase;

/**
 * Created a class to handle the divide process
 */
public class Divide extends CalculateBase {
	
	// Constructs
    public Divide() {
    }
    public Divide(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }
    
    
    @Override
    public void calculate() {
        double value = getLeftVal() / getRightVal();
        setResult(value);
        ;
    }
}

package com.lousite.basic.math;

/**
 * Create CalculatorInterface used to in force user rules for this particular interface  
 */
public interface CalculatorInterface {
	// Define needed constant(s)
    String SEPARATOR = " ";
    
    // Define empty methods, forcing classes implementing this interface to apply it
    String getKeyword();     // example = add, subtract, multiply, divide, etc.... 
    char getSymbol();        // output symbol to use - example - +, -, *, /, etc.... 
    double doCalculation(double leftVal, double rightVal);
}
